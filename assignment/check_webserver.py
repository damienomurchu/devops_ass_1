#!/usr/bin/python3
# script to check if Nginx service is running. If not, it will attempt to be started

import subprocess
import sys

def checknginx():
  cmd = 'ps -A | grep nginx | grep -v grep'

  (status, output) = subprocess.getstatusoutput(cmd)

  if status > 0:
    print("Nginx Server IS NOT running. Will attempt to start it")
    startnginx()
  else:
    print("Nginx Server IS running")


def startnginx():
  cmd = 'ps -A | grep nginx | grep -v grep'

  (status, output) = subprocess.getstatusoutput(cmd)

  if status == 0:
    print("Nginx server is already running")
    sys.exit(1)
  else:
    sys.stderr.write(output)
    print("Starting Nginx...")
    cmd = 'sudo service nginx start'
    (status, output) = subprocess.getstatusoutput(cmd)
    if status:
      print("--- Error starting nginx! ---")
      sys.exit(2)
    print("Nginx started successfully")
    sys.exit(0)


def main():
    checknginx()

if __name__ == '__main__':
  main()