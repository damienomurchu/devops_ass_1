#!/usr/bin/python3

# class represents an Amazon AWS user's details
class user:
  def __init__(self, connRegion, pemKey):
    self.connRegion = connRegion
    self.pemKey = pemKey

  # returns the users AWS connection region
  def getConn(self):
    return self.connRegion

  # returns just the name of the users .pem key
  def getPemKey(self):
    return self.pemKey