# DevOps AWS Assignment #

## WHAT IS THIS REPO ? ##
An assortment of python scripts that manipulate AWS services via the Boto API. 
All scripts are written and intended to be run in python3 and are intended to 
illustrate how distributed computing resources and infrastructure may be both 
automated and managed via code.

## SCRIPTS ##

All core scripts are in assignment folder. Misc folder contains unused scripts, and exercises folder contains some supplied code snippets.
* run_newwebserver.py is the main script, creates a new instance, preps it with nginx, and runs the check_webserver.py script to check nginx
* check_webserver.py checks if nginx is running on an instance, and if not, starts it
* instance.py is a helper class that creates objects representing amazon instances
* connUtils.py is a helper class of connection-related helper methods to transact with an aws connection
* ssh.py provides assorted helper methods to remotely interact with AWS instances via ssh

## USAGE NOTES ##
a .boto file with your AWS connection credentials should be present in the root of your home directory
your .pem key should also be located in the root of your home directory, as this is where the script will look for it